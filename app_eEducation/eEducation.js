const ETABLISHMENT_ID = "school-387"

function getFormInfo() {
    
    let educationalEvent = {}
    educationalEvent.etablishmentId = ETABLISHMENT_ID
    educationalEvent.date = document.getElementById('date').value
    educationalEvent.type = document.getElementById('type').value
    educationalEvent.description = document.getElementById('description').value
        
    return educationalEvent
}

function fillForm() {
    
    // educational info
    document.getElementById('date').value = '2019-09-29'
    document.getElementById('type').value = 'Diploma'
    document.getElementById('description').value = 'Ph.D quantum mathematics.\n\nJury congratulations.'
}

function submit() {
    let record = getFormInfo()
   
    $.ajax({
       
          url : 'http://localhost:3000/api/civilStatus',
          type : 'POST', 
          data : JSON.stringify(record),
          dataType : 'text',
          contentType: "application/json; charset=utf-8",
          success : function(code_html, statut){ // success est toujours en place, bien sûr !
       alert('OK')
                     },
   
   
          error : function(resultat, statut, erreur){
       alert(erreur)
   
          }
       })
   
   }
