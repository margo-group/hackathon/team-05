import 'dotenv/config';
import {Application} from './application';
import { EnvironmentUtils } from './utils/environment.utils';

// validate required environment variables
EnvironmentUtils.validate();

// the application entrypoint
new Application();