import mongoose, { Document } from 'mongoose';
import { HealthRecordIpfs } from '../models/health-record-ipfs.model';
import { DocumentUtils } from '../../utils/document.utils';
import { HealthRecord } from '../models/health-record.model';

class HealthRecordRepository {
    private healthRecordModel: mongoose.Model<HealthRecordIpfs & mongoose.Document, {}>;

    private schema = new mongoose.Schema({
        unid: {
            type: String,
            unique: true
        },
        blockchainAddress: {
            type: String,
            unique: true
        },
        ipfsHash: {
            type: String,
            unique: false
        }
    });

    public constructor() {
        this.healthRecordModel = mongoose.model<HealthRecordIpfs & mongoose.Document>('HealthRecordIpfs', this.schema);
    }

    /**
     * Put a health record ipfs into database
     * @param healthRecordIpfs 
     */
    public add(healthRecordIpfs: HealthRecordIpfs): Promise<HealthRecordIpfs> {
        let healthRecordIpfsEntity: HealthRecordIpfs & mongoose.Document = new this.healthRecordModel(healthRecordIpfs);
        return new Promise<HealthRecordIpfs>((resolve, reject) => {
            healthRecordIpfsEntity.save().then((value: HealthRecordIpfs & mongoose.Document) => {
                resolve(DocumentUtils.toModel<HealthRecordIpfs>(value.toJSON()));
            }).catch((reason) => {
                reject(`Health Record ipfs cannot be registered.\nreason : ${reason}`);
            });
        });
    }

}

export const healthRecordRepository = new HealthRecordRepository();