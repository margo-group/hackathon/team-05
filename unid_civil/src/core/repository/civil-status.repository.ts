import mongoose, { Document } from 'mongoose';
import { CivilStatus } from '../models/civil-status.model';
import { DocumentUtils } from '../../utils/document.utils';
import { ArrayUtils } from '../../utils/array.utils';
import { CivilStatusCriteria } from '../models/civil-status-criteria.model';

class CivilStatusRepository {
    private civilStatusModel: mongoose.Model<CivilStatus & mongoose.Document, {}>;

    private schema = new mongoose.Schema({
        unid: {
            type: String,
            unique: true
        },
        blockchainAddress: {
            type: String,
            unique: true
        },
        issuerId: {
            type: String,
            unique: false
        },
        child: {
            type: mongoose.SchemaTypes.Mixed,
            unique: false
        },
        mother: {
            type: mongoose.SchemaTypes.Mixed,
            unique: false
        },
        father: {
            type: mongoose.SchemaTypes.Mixed,
            unique: false
        },
        witnesses: {
            type: mongoose.SchemaTypes.Mixed,
            unique: false
        }
    });

    public constructor() {
        this.civilStatusModel = mongoose.model<CivilStatus & mongoose.Document>('CivilStatus', this.schema);
    }

    /**
     * Put a civil status into database
     * @param civilStatus 
     */
    public add(civilStatus: CivilStatus): Promise<CivilStatus> {
        let civilStatusEntity: CivilStatus & mongoose.Document = new this.civilStatusModel(civilStatus);
        return new Promise<CivilStatus>((resolve, reject) => {
            civilStatusEntity.save().then((value: CivilStatus & mongoose.Document) => {
                resolve(DocumentUtils.toModel<CivilStatus>(value.toJSON()));
            }).catch((reason) => {
                reject(`Civil status cannot be registered.\nreason : ${reason}`);
            });
        });
    }

    /**
     * Find civil status by unid
     * @param unid : the civil status unid 
     */
    public async findByUnid(unid: string): Promise<CivilStatus> {
        let query = this.civilStatusModel.find({ unid: unid });
        return new Promise<CivilStatus>((resolve, reject) => {
            query.exec().then((values: (CivilStatus & mongoose.Document)[]) => {
                if (ArrayUtils.isEmpty(values)) {
                    resolve();
                } else {
                    let civilStatus = DocumentUtils.toModel<CivilStatus>(values[0].toJSON());
                    resolve(civilStatus);
                }
            }).catch(() => {
                console.log(`Not found for unid ${unid}`);
                reject();
            });
        });
    }

    /**
     * Find civil status by criteria
     * 
     * @param civilStatus : the civil status criteria 
     */
    public async search(searchCriteria: CivilStatusCriteria): Promise<CivilStatus[]> {
        let criterias: any = {};
        for(let attr in searchCriteria) {
            if((<any>searchCriteria)[attr] !== undefined) {
                criterias[`child.${attr}`] = (<any>searchCriteria)[attr];
            }
        }

        let query = this.civilStatusModel.find(criterias);
        return new Promise<CivilStatus[]>((resolve, reject) => {
            query.exec().then((values: (CivilStatus & mongoose.Document)[]) => {
                if (ArrayUtils.isEmpty(values)) {
                    resolve();
                } else {
                    const results: Array<CivilStatus> = values.map((value) => DocumentUtils.toModel<CivilStatus>(value.toJSON()));
                    resolve(results);
                }
            }).catch(() => {
                reject();
            });
        });
    }
}

export const civilStatusRepository = new CivilStatusRepository();