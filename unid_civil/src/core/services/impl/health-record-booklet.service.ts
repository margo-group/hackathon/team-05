import { IHealthRecordBookletService } from "../health-record-booklet.service";
import { HealthRecord } from "../../models/health-record.model";
import { HealthRecordIpfs } from "../../models/health-record-ipfs.model";
import { StringUtils } from "../../../utils/string.utils";
import { ipfsService } from "./ipfs.service";
import { healthRecordRepository } from "../../repository/health-record.repository";
import { blockchainService } from "./blockchain.service";
import { ObjectUtils } from "../../../utils/object.utils";


class HealthRecordBookletServiceImpl implements IHealthRecordBookletService {
    public static readonly BLOCKCHAIN_CONTRACT_NAME: string = 'unid_health';

    add(healthRecord: HealthRecord) : Promise<HealthRecord> {
        const healthRecordBuffer = JSON.stringify(healthRecord)
        let healthRecordIpfs: HealthRecordIpfs = {
            unid: healthRecord.unid
        };

        return new Promise<any>((resolve, reject) => {
            ipfsService.addFile(Buffer.from(healthRecordBuffer)).then((hash) => {
                blockchainService.publishContract(HealthRecordBookletServiceImpl.BLOCKCHAIN_CONTRACT_NAME, []).then((address) => {
                    if(StringUtils.isNotBlank(address)) {
                        healthRecordIpfs.ipfsHash = hash[0].hash;
                        healthRecordIpfs.blockchainAddress = address;
                        healthRecordRepository.add(healthRecordIpfs).then((data) => {
                            if(ObjectUtils.isNotNullOrUndefined(data)) {
                                resolve(healthRecordIpfs);
                            } else {
                                reject('Could not store data into internal registry');
                            }
                        }).catch((reason) => reject(reason));
                    }
                }).catch((reason) => reject(reason));
            });
        });
    }
}

export const healthRecordBookletService = new HealthRecordBookletServiceImpl();