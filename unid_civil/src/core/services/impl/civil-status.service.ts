import { ICivilStatusService } from "../civil-status.service";
import { CivilStatus } from "../../models/civil-status.model";
import { civilStatusRepository } from "../../repository/civil-status.repository";
import { StringUtils } from "../../../utils/string.utils";
import { Guid } from 'guid-typescript';
import { blockchainService } from "./blockchain.service";
import { CryptoUtils } from "../../../utils/crypto.utils";
import { ObjectUtils } from "../../../utils/object.utils";
import { CivilStatusCriteria } from "../../models/civil-status-criteria.model";
import { Person } from "../../models/person.model";

class CivilStatusServiceImpl implements ICivilStatusService {
    public static readonly BLOCKCHAIN_CONTRACT_NAME: string = 'unid_civil';
    public static readonly BLOCKCHAIN_CONTRACT_CHILD_FIELD: string = 'civilStatusHash';

    add(civilStatus: CivilStatus) : Promise<CivilStatus> {
        if(StringUtils.isBlank(civilStatus.unid)) {
            civilStatus.unid = Guid.create().toString();
        }

        const witness1: string = ObjectUtils.isNotNullOrUndefined(civilStatus.witnesses) ? StringUtils.defaultIfBlank(civilStatus.witnesses.first) : '';
        const witness2: string = ObjectUtils.isNotNullOrUndefined(civilStatus.witnesses) ? StringUtils.defaultIfBlank(civilStatus.witnesses.second) : '';

        const data: Array<string> = [
            civilStatus.unid,
            CryptoUtils.sha256(JSON.stringify(civilStatus.child)),
            CryptoUtils.sha256(JSON.stringify(civilStatus.mother)),
            CryptoUtils.sha256(JSON.stringify(civilStatus.father)),
            civilStatus.issuerId,
            CryptoUtils.sha256(JSON.stringify(witness1)),
            CryptoUtils.sha256(JSON.stringify(witness2))
        ];

        return new Promise((resolve, reject) => {
            blockchainService.publishContract(CivilStatusServiceImpl.BLOCKCHAIN_CONTRACT_NAME, data).then((address) => {
                if(StringUtils.isNotBlank(address)) {
                    civilStatus.blockchainAddress = address;
                    civilStatusRepository.add(civilStatus).then((data) => {
                        if(ObjectUtils.isNotNullOrUndefined(data)) {
                            resolve(civilStatus);
                        } else {
                            reject('Could not store data into internal registry');
                        }
                    }).catch((reason) => reject(reason));
                }
            }).catch((reason) => reject(reason));
        });
    }

    update(civilStatus?: any) : Promise<CivilStatus> {
        return civilStatusRepository.add(civilStatus);
    }

    findByUnid(unid: string): Promise<CivilStatus> {
        return civilStatusRepository.findByUnid(unid);
    }

    checkIntegrity(contractAddress: string, person: Person): Promise<boolean> {
        const dataMap: Map<string, any> = new Map<string, any>();
        dataMap.set(CivilStatusServiceImpl.BLOCKCHAIN_CONTRACT_CHILD_FIELD, person);

        return blockchainService.checkIntegrity(CivilStatusServiceImpl.BLOCKCHAIN_CONTRACT_NAME, contractAddress, dataMap);
    }

    /**
     * Find civil status by criteria
     * 
     * @param civilStatus : the civil status criteria 
     */
    public search(searchCriteria: CivilStatusCriteria): Promise<CivilStatus[]> {
        return civilStatusRepository.search(searchCriteria);
    }
}

export const civilStatusService = new CivilStatusServiceImpl();