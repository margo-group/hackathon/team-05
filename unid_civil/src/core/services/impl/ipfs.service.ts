import { IPFSService } from "../ipfs.service";
import dotenv from "dotenv";
const ipfsAPI = require('ipfs-api');
class IPFSServiceImpl implements IPFSService {
    private ipfs: any;

    constructor() {
        //Connecting to the ipfs network via infura gateway
        this.ipfs = ipfsAPI(process.env.IPFS_GATEWAY_HOST, process.env.IPFS_GATEWAY_PORT, { protocol:  process.env.IPFS_GATEWAY_PROTOCOL })
    }
    /**
     * Add file to IPFS
     * 
     * @param fileBuffer : buffer containing file data to add to IPFS
     * 
     */
    addFile(fileBuffer: Buffer): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                console.log('add file to IPFS')
                this.ipfs.files.add(fileBuffer).then((hash: string) => {
                    resolve(hash)
                }).catch((reason: any) => {
                    reject(reason)
                })
            } catch (e) {
                // transaction cannot be executed for some reasons
                reject(`Error adding file to IPFS : ${e}`);
            }
        })
    }
}

export const ipfsService: IPFSServiceImpl = new IPFSServiceImpl();
