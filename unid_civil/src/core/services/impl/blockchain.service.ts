import { BlockchainService } from "../blockchain.service";
import dotenv from "dotenv";
import { FileUtils } from "../../../utils/file.utils";
import Web3 from "web3";
import { TransactionObject } from "web3/eth/types";
import { ObjectUtils } from "../../../utils/object.utils";
import { CryptoUtils } from "../../../utils/crypto.utils";
import { StringUtils } from "../../../utils/string.utils";
import { MapUtils } from "../../../utils/map.utils";

const Tx = require('ethereumjs-tx');

class BlockchainServiceImpl implements BlockchainService {
    private web3: Web3;

    constructor() {
        this.web3 = new Web3(process.env.BLOCHAIN_ETHEREUM_URL);
    }

    /**
     * Publish a contract into ethereum blockchain
     * 
     * @param contractName : the name of the contract to publish
     * @param args : the contract arguments
     */
    publishContract(contractName: string, args: Array<any>): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                const account = <string>process.env.BLOCKCHAIN_ACCOUNT;
                const privKey: Buffer = Buffer.from('' + process.env.BLOCKCHAIN_PRIV_KEY, 'hex');

                const bytecode: any = FileUtils.readFile(`assets/interfaces/${contractName}/${contractName}.bytecode`);
                this.web3.eth.getTransactionCount(account).then((txCount: number) => {
                    const txObject = {
                        nonce: this.web3.utils.toHex(txCount),
                        // Raise the gas limit to a much higher amount
                        gasLimit: this.web3.utils.toHex(1000000),
                        gasPrice: this.web3.utils.toHex(this.web3.utils.toWei('10', 'gwei')),
                        data: bytecode,
                        arguments: args
                    };

                    const tx = new Tx(txObject);
                    tx.sign(privKey);

                    const serializedTx = tx.serialize();
                    const raw = '0x' + serializedTx.toString('hex');
                    this.web3.eth.sendSignedTransaction(raw).then((data) => {
                        resolve(data.contractAddress);
                    }).catch((reason) => {
                        // transaction failed
                        reject(`Cannot send signed transaction.\nreason : ${reason}`);
                    });
                }).catch((reason) => reject(`Cannot create signed transaction.\nreason : ${reason}`));
            } catch (e) {
                // transaction cannot be executed for some reasons
                reject(`Error pending block chain connection : ${e}`);
            }
        });
    }

    checkIntegrity(contractName: string, contractAdress: string, dataMap: Map<string, any>): Promise<boolean> {
        return new Promise((resolve, reject) => {
            if (MapUtils.isEmpty(dataMap)) {
                reject('The data map to check cannot be empty.');
                return;
            }

            const abi: any = FileUtils.readJsonFile(`assets/interfaces/${contractName}/${contractName}.json`);
            const contract = new this.web3.eth.Contract(abi, contractAdress);

            new Promise<boolean>(async (resolve1, reject1) => {
                const resultMap: Map<string, boolean> = new Map<string, boolean>();
                for (let entry of Array.from(dataMap.entries())) {
                    let key = entry[0];
                    let data = entry[1];
                    let strData: string = (typeof data === 'string') ? data : JSON.stringify(data);

                    try {
                        let value: string = await contract.methods[key].call(this).call();
                        resultMap.set(key, StringUtils.equals(value, CryptoUtils.sha256(strData)));
                    } catch (e) {
                        resultMap.set(key, false);
                        console.log(e);
                    }
                }

                // compute all map result
                let integrityCheckResult: boolean = true;
                for (let entry of Array.from(resultMap.entries())) {
                    let isValidIntegrity: boolean = entry[1];
                    integrityCheckResult = integrityCheckResult && isValidIntegrity;
                }

                resolve1(integrityCheckResult);
            })
                .then((result: boolean) => resolve(result))
                .catch((reason) => reject(`Cannot check integrity for following reasons :\n${reason}`));
        });
    }
}

export const blockchainService: BlockchainServiceImpl = new BlockchainServiceImpl();