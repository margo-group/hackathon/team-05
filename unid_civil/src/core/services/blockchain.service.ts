export interface BlockchainService {
    /**
     * Publish a contract into ethereum blockchain
     * 
     * @param contractName : the name of the contract to publish
     * @param args : the contract arguments
     */
    publishContract(contractName: string, args: Array<any>): Promise<any>;

    /**
     * Check data integrity with blockchain smart contract
     * 
     * @param contractName : the smart contract name
     * @param contractAdress : the smart contract address
     * @param dataMap : the data which contains smart contract data name with associated data to check
     */
    checkIntegrity(contractName: string, contractAdress: string, dataMap: Map<string, any>): Promise<boolean>;
}