import { HealthRecord } from "../models/health-record.model";


export interface IHealthRecordBookletService {
    /**
     * add Health Record into IPFS
     * @param healthRecord 
     */
    add(healthRecord: HealthRecord) : Promise<HealthRecord>
}