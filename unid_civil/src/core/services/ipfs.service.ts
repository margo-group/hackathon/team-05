export interface IPFSService {
    /**
     * Add file to IPFS
     * 
     * @param fileBuffer : buffer containing file data to add to IPFS
     * 
     */
    addFile(fileBuffer: Buffer): Promise<any>;
}