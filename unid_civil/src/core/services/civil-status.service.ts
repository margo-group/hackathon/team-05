import { CivilStatus } from "../models/civil-status.model";
import mongoose from "mongoose";
import { CivilStatusCriteria } from "../models/civil-status-criteria.model";
import { Person } from "../models/person.model";

export interface ICivilStatusService {
    /**
     * Put a civil status into database
     * @param civilStatus 
     */
    add(civilStatus: CivilStatus) : Promise<CivilStatus>

    /**
     * Update a civil status into database
     * @param civilStatus 
     */
    update(civilStatus: CivilStatus) : Promise<CivilStatus>

    /**
     * Find civil status by unid
     * @param unid : the civil status unid 
     */
    findByUnid(unid: string): Promise<CivilStatus>;

    /**
     * Find civil status by criteria
     * 
     * @param civilStatus : the civil status criteria 
     */
    search(searchCriteria: CivilStatusCriteria): Promise<CivilStatus[]>;

    /**
     * Check data integrity with blockchain smart contract
     * 
     * @param contractAddress : the smart contract address
     * @param person : the person data to check
     */
    checkIntegrity(contractAddress: string, person: Person): Promise<boolean>;
}