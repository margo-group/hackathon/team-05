import { Person } from "./person.model";

export interface CivilStatus {
    unid: string;
    issuerId: string;
    child: Person;
    mother: Person;
    father: Person;
    witnesses: any;
    blockchainAddress: string;
}