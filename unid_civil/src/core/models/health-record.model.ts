export interface HealthRecord {
    unid: string;
    etablishmentId: string;
    date: Date;
    type: string;
    description: string;
}
