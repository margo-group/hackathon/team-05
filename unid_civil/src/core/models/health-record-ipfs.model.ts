export interface HealthRecordIpfs {
    unid: string;
    blockchainAddress?: string;
    ipfsHash?: string;
}
