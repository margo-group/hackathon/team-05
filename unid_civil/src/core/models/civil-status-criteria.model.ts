import { Person } from "./person.model";

export interface CivilStatusCriteria extends Person {
    unid: string;
}