import { Gender } from "./gender.enum";

export class Person {
    gender?: Gender;
    firstname?: string;
    lastname?: string;
    birthdate?: Date;
    birthplace?: string;
}