import express from 'express';
import * as bodyParser from 'body-parser';
import dotenv from "dotenv";
import { RoutesConfigurer } from "./config/routes.config";
import { LoggerMiddleware } from './middleware/logger.middleware';
import mongoose from 'mongoose';
import { StringUtils } from './utils/string.utils';
import cors from 'cors';

export class Application {
    private app: express.Application;

    public constructor() {
        this.app = express();

        // initialize configuration
        dotenv.config();

        // initialize database connection
        this.dbConnect();

        // register middleware
        this.registerMiddleware();

        // Configure routes
        new RoutesConfigurer(this.app);

        // start server
        this.listen();
    }

    /**
     * Init database connection
     */
    private dbConnect(): void {
        const {
            MONGO_USER,
            MONGO_PASSWORD,
            MONGO_PATH,
        } = process.env;

        let url: string = `mongodb://${MONGO_PATH}`;
        if(StringUtils.isNotBlank(MONGO_USER || "")) {
            url = `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_PATH}`;
        }

        mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true});
    }

    /**
     * Register all middleware
     */
    private registerMiddleware(): void {
        this.app.use(LoggerMiddleware.log);
        this.app.use(bodyParser.json());

        this.app.use(bodyParser.urlencoded({
            extended: true
        }))
        
        this.app.use(cors({
            origin: true,
            methods: 'OPTIONS,GET,PUT,POST,DELETE',
            allowedHeaders: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept',
            credentials: true
        }));
    }

    /**
     * Start the api server
     */
    private listen(): void {
        const port = process.env.SERVER_PORT || 3000;
        this.app.listen(port, () => {
            console.log(`Server running and listening on port ${port}`);
        });
    }
}