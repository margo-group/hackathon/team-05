/**
 * Map utils class to manipulate maps
 */
export class MapUtils {
    /**
     * Constructor
     */
    private constructor() {

    }

    /**
     * Indicate if an map is empty
     * 
     * @param value : the map to check
     * @return boolean
     */
    public static isEmpty(value: Map<any, any>): boolean {
        return (null === value || undefined === value || value.size === 0);
    }

    /**
     * Indicate if an map is not empty
     * 
     * @param value : the map to check
     * @return boolean
     */
    public static isNotEmpty(value: Map<any, any>): boolean {
        return !MapUtils.isEmpty(value);
    }

    /**
     * Indicate if an map is not empty
     * 
     * @param value : the map to check
     * @return number which represent size
     */
    public static size(value: Map<any, any>): number {
        if (null === value || undefined === value) {
            return 0;
        }

        return value.size;
    }

    /**
     * Indicate if an map contains a value
     * 
     * @param map : the map
     * @param key : the key to find
     * @return boolean
     */
    public static containsKey(map: Map<any, any>, key: any): boolean {
        return (MapUtils.isNotEmpty(map) && map.has(key));
    }

    /**
     * Clear map and allow to keep same reference
     * 
     * @param map : the map to clear
     */
    public static clear(map: Map<any, any>): void {
        if (MapUtils.isNotEmpty(map)) {
            map.clear();
        }
    }

    /**
     * Remove object
     * 
     * @param map : the map to search in
     * @param searchKey : the searched key
     */
    public static remove(map: Map<any, any>, searchKey: any): void {
        if (MapUtils.isNotEmpty(map) && searchKey !== null && searchKey !== undefined) {
            map.delete(searchKey);
        }
    }
}