import { ArrayUtils } from './array.utils';

/**
 * Reflection utils class
 */
export class ReflectionUtils {
    /**
     * Constructor
     */
    private constructor() {

    }

    /**
     * Get attribute list of an object
     * 
     * @param obj : the object to analyze
     * @return list of attributes
     */
    public static getAttributes<T>(obj: T) : Array<string> {
        let attributeList: Array<string> = new Array<string>();
        const objectKeys = Object.keys(obj) as Array<keyof T>;
        for (let key of objectKeys) {
            if (typeof obj[key] !== "function") {
                attributeList.push(key.toString());
            }
        }

        return attributeList;
    }

}