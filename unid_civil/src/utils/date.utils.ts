import { ObjectUtils } from './object.utils';
import { StringUtils } from './string.utils';
import { isNumber } from 'util';

/**
 * Date utils class
 */
export class DateUtils {
    /**
     * The posix year
     */
    public static readonly POSIX_YEAR: number = 1970;

    /**
     * The posix year
     */
    public static readonly DEFAULT_DATE_SEPARATOR_CHAR: string = "/";

    /**
     * Constructor
     */
    private constructor() {

    }

    /**
     * Get the age since date from today
     * 
     * @param since : the since date
     * 
     * @return the age
     */
    public static getAge(since: Date): number {
        return DateUtils.getAgeBetween(since, new Date());
    }

    /**
     * Get the age since date from fromDate
     * 
     * @param since : the since date
     * @param from : the from date
     * 
     * @return the age
     */
    public static getAgeBetween(since: Date, from: Date): number {
        if (ObjectUtils.isNullOrUndefined(since) || ObjectUtils.isNullOrUndefined(from)) {
            return 0;
        }

        let timestampDiff: number = from.getTime() - since.getTime();
        let ageDate: Date = new Date(timestampDiff);
        return Math.abs(ageDate.getFullYear() - DateUtils.POSIX_YEAR);
    }

    /**
     * Convert string to Date
     * 
     * @param strDate : the string date representation
     * @param separator : the date separator
     */
    public static parse(strDate: string, separator: string = ""): Date {
        if (StringUtils.isBlank(separator)) {
            separator = DateUtils.DEFAULT_DATE_SEPARATOR_CHAR;
        }

        if (separator !== DateUtils.DEFAULT_DATE_SEPARATOR_CHAR) {
            StringUtils.replace(strDate, separator, DateUtils.DEFAULT_DATE_SEPARATOR_CHAR);
        }

        let partArray: Array<string> = strDate.trim().split(separator);

        const year = Number(partArray[2]);
        const month = Number(partArray[1]) - 1;
        const date = Number(partArray[0]);

        if(isNumber(year) && isNumber(month) && isNumber(date)) {
            const parsedDate = new Date(year, month, date);
            if(parsedDate.getDate() === date) {
                return parsedDate;
            }
        }
        throw new Error("Invalid date");
    }
}