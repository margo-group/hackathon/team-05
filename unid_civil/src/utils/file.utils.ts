import * as fs from 'fs';

export class FileUtils {
    private constructor() {

    }

    /**
     * Read file content as string
     * 
     * @param path : The file path
     */
    public static readFile(path: string): string {
        return fs.readFileSync(path, 'utf8');
    }

    /**
     * Read json file and convert to object
     * 
     * @param path : the file path
     */
    public static readJsonFile<T>(path: string): T {
        return <T>JSON.parse(fs.readFileSync(path, 'utf8'));
    }
}