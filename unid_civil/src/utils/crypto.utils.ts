import { createHash } from "crypto";

export class CryptoUtils {
    private constructor() {

    }

    /**
     * Get sha 256 hash of a value
     * 
     * @param value : the value to hash
     * @return the hashed value
     */
    public static sha256(value: string): string {
        return createHash('sha256').update(value).digest('hex');
    }
}