import { StringUtils } from './string.utils';

/**
 * Object utils class
 */
export class ObjectUtils {
    /**
     * Constructor
     */
    private constructor() {

    }

    /**
     * Check if object is null or undefined
     * 
     * @param obj : the object to check
     * 
     * @return true if is null or undefined otherwise false
     */
    public static isNullOrUndefined(obj: any): boolean {
        return (null === obj || undefined === obj);
    }

    /**
     * Check if object is not null or undefined
     * 
     * @param obj : the object to check
     * 
     * @return true if is not null or undefined otherwise false
     */
    public static isNotNullOrUndefined(obj: any): boolean {
        return !ObjectUtils.isNullOrUndefined(obj);
    }


    /**
     * Transform object to uri parameters string
     * 
     * @param data : data object to transform
     * @return uri params or empty string if data is null or undefined
     */
    public static toUriParameters(data: any): string {
        if (ObjectUtils.isNullOrUndefined(data)) {
            return '';
        }

        return Object.entries(data).map(e => e.map<string>((value: any) => StringUtils.isNotEmpty(value) ? encodeURIComponent(value) : '').join('=')).join('&');
    }
}