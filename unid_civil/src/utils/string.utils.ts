/**
 * String utils class to manipulate strings
 */
export class StringUtils {
    /**
     * Constructor
     */
    private constructor() {

    }

    /**
     * Indicate if a string is empty
     * 
     * @param value : the value to check
     * @return boolean
     */
    public static isEmpty(value: string): boolean {
        return typeof value !== 'string' || (null === value || undefined === value || value.length === 0);
    }

    /**
     * Indicate if a string is not empty
     * 
     * @param value : the value to check
     * @return boolean
     */
    public static isNotEmpty(value: string): boolean {
        return !StringUtils.isEmpty(value);
    }

    /**
     * Indicate if a string is blank
     * 
     * @param value : the value to check
     * @return boolean
     */
    public static isBlank(value: string): boolean {
        return typeof value !== 'string' || (null === value || undefined === value || value.trim().length === 0);
    }

    /**
     * Indicate if a string is not blank
     * 
     * @param value : the value to check
     * @return boolean
     */
    public static isNotBlank(value: string): boolean {
        return !StringUtils.isBlank(value);
    }

    /**
     * Format a string with args parameters
     * 
     * @param value : the string value to format
     * @param args : the args to replace into string
     * @return formated string
     */
    public static format(value: string, ...args: string[]): string {
        if (StringUtils.isNotBlank(value)) {
            for (let index = 0; index < args.length; index++) {
                value = value.replace(`{${index}}`, args[index]);
            }
        }
        return value;
    }

    /**
     * Replace string by another string into the value
     * 
     * @param value : the string value to treat
     * @param searchValue : the value to replace
     * @param by : the new value to put in place of searchValue
     * 
     * @return the new value
     */
    public static replace(value: string, searchValue: string, by: string): string {
        if (StringUtils.isNotBlank(value) && StringUtils.isNotBlank(searchValue)) {
            value = value.replace(searchValue, StringUtils.defaultIfEmpty(by));
        }
        return value;
    }

    /**
     * Get default string if value is empty
     * 
     * @param value : the string value to treat
     * @param defaultValue : the default value
     * 
     * @return the new value. If default value is not set then return empty string
     */
    public static defaultIfEmpty(value: string, defaultValue: string = ""): string {
        if (StringUtils.isEmpty(value)) {
            return (StringUtils.isBlank(defaultValue)) ? "" : defaultValue;
        }
        return value;
    }

    /**
     * Get default string if value is blank
     * 
     * @param value : the string value to treat
     * @param defaultValue : the default value
     * 
     * @return the new value. If default value is not set then return empty string
     */
    public static defaultIfBlank(value: string, defaultValue: string = ""): string {
        if (StringUtils.isBlank(value)) {
            return (StringUtils.isBlank(defaultValue)) ? "" : defaultValue;
        }
        return value;
    }

    /**
     * Compare 2 string with ignore case and diacritics removed
     * 
     * @param value1 : the first value
     * @param value2 : the second value
     */
    public static equalsIgnoreCaseIgnoreAccent(value1: string, value2: string): boolean {
        if ((StringUtils.isBlank(value1) && StringUtils.isNotBlank(value2))
            || (StringUtils.isBlank(value2) && StringUtils.isNotBlank(value1))) {
            return false;
        }

        if (StringUtils.isBlank(value1) && StringUtils.isBlank(value2)) {
            return true;
        }

        let normalizedValue1: string = StringUtils.normalize(value1);
        let normalizedValue2: string = StringUtils.normalize(value2);
        return (normalizedValue1 === normalizedValue2);
    }

    /**
     * Compare 2 string with ignore case
     * 
     * @param value1 : the first value
     * @param value2 : the second value
     */
    public static equalsIgnoreCase(value1: string, value2: string): boolean {
        if ((StringUtils.isBlank(value1) && StringUtils.isNotBlank(value2))
            || (StringUtils.isBlank(value2) && StringUtils.isNotBlank(value1))) {
            return false;
        }

        if (StringUtils.isBlank(value1) && StringUtils.isBlank(value2)) {
            return true;
        }

        return (value1.toLowerCase() === value2.toLowerCase());
    }

    /**
     * Compare 2 string
     * 
     * @param value1 : the first value
     * @param value2 : the second value
     */
    public static equals(value1: string, value2: string): boolean {
        if ((StringUtils.isBlank(value1) && StringUtils.isNotBlank(value2))
            || (StringUtils.isBlank(value2) && StringUtils.isNotBlank(value1))) {
            return false;
        }

        if (StringUtils.isBlank(value1) && StringUtils.isBlank(value2)) {
            return true;
        }

        return (value1 === value2);
    }

    /**
     * Compare 2 string with ignore case and diacritics removed
     * 
     * @param value1 : the first value
     * @param value2 : the second value
     */
    public static containsIgnoreCaseIgnoreAccent(value1: string, value2: string): boolean {
        let normalizedValue1: string = StringUtils.defaultIfBlank(StringUtils.normalize(value1));
        let normalizedValue2: string = StringUtils.defaultIfBlank(StringUtils.normalize(value2));

        return (normalizedValue1.trim().toLowerCase().indexOf(normalizedValue2.trim().toLowerCase()) !== -1);
    }

    /**
     * Normalize string and remove diacritics (accent and cedil)
     * 
     * @param value : the value to normalize
     */
    public static normalize(value: string): string {
        return value.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
    }

    /**
     * Transform value to lower case
     * 
     * @param value : the value to transform
     * @return lowercase value if is not blank
     */
    public static toLowercase(value: string): string {
        return (StringUtils.isBlank(value)) ? value : value.toLowerCase();
    }

    /**
     * Transform value to upper case
     * 
     * @param value : the value to transform
     * @return uppercase value if is not blank
     */
    public static toUppercase(value: string): string {
        return (StringUtils.isBlank(value)) ? value : value.toUpperCase();
    }
}