/**
 * Array utils class to manipulate arrays
 */
export class ArrayUtils {
    /**
     * Constructor
     */
    private constructor() {

    }

    /**
     * Indicate if an array is empty
     * 
     * @param value : the array to check
     * @return boolean
     */
    public static isEmpty(value: Array<any>): boolean {
        return (null === value || undefined === value || value.length === 0);
    }

    /**
     * Indicate if an array is not empty
     * 
     * @param value : the array to check
     * @return boolean
     */
    public static isNotEmpty(value: Array<any>): boolean {
        return !ArrayUtils.isEmpty(value);
    }

    /**
     * Indicate if an array is not empty
     * 
     * @param value : the array to check
     * @return number which represente size
     */
    public static size(value: Array<any>): number {
        if(null === value || undefined === value) {
            return 0;
        }

        return value.length;
    }

    /**
     * Indicate if an array contains a value
     * 
     * @param arr : the array
     * @param value : the value to find
     * @return boolean
     */
    public static contains(arr: Array<any>, value:any): boolean {
        return (ArrayUtils.isNotEmpty(arr) && arr.indexOf(value) !== -1);
    }

    /**
     * Clear array and allow to keep same reference
     * 
     * @param arr : the array to clear
     */
    public static clear(arr: Array<any>): void {
        if(ArrayUtils.isNotEmpty(arr)) {
            while(arr.length > 0) {
                arr.shift();
            }
        }
    }

    /**
     * Insert element at position into array
     * 
     * @param arr : the array to update
     * @param value : the value to insert
     * @param position : the position at the value must be inserted
     */
    public static insertAt(arr: Array<any>, value: any, position: number) {
        if(null === value || undefined === value) {
            throw new Error(`Cannot insert element at ${position} because your array is null or undefined`);
        }

        arr.splice(position, 0, value);
    }
}