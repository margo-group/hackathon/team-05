import { cleanEnv, str, port } from 'envalid';

export class EnvironmentUtils {
    private constructor() {

    }

    /**
     * Validate the environment variables
     */
    public static validate(): void {
        cleanEnv(process.env, {
            MONGO_PASSWORD: str(),
            MONGO_PATH: str(),
            MONGO_USER: str(),
            SERVER_PORT: port(),
            BLOCKCHAIN_ACCOUNT: str(),
            BLOCHAIN_ETHEREUM_URL: str()
        });
    }
}