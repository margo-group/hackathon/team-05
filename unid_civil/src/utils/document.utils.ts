import mongoose from "mongoose";

export class DocumentUtils {
    private constructor() {

    }

    public static toModel<T>(document: T & mongoose.Document) : T {
        let obj = document;
        delete obj._id;
        delete obj.__v;
        return <T> obj;
    }
}