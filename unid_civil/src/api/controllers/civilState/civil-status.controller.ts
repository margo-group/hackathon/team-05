import { Router, Request, Response } from "express";
import { AbstractController } from "../abstract.controller";
import { civilStatusService } from "../../../core/services/impl/civil-status.service";
import { ObjectUtils } from "../../../utils/object.utils";
import { ArrayUtils } from "../../../utils/array.utils";
import { CivilStatus } from "../../../core/models/civil-status.model";
import { blockchainService } from "../../../core/services/impl/blockchain.service";
import { Person } from "../../../core/models/person.model";

/**
 * The civil state api to add, update, delete entries into civil state
 */
export class CivilStatusController extends AbstractController {
    private static readonly BASE_PATH: string = '/civilStatus';

    /**
     * Configure routing
     * 
     * @param router : the express router instance
     */
    configureRoutes(router: Router): void {
        router.get(`${CivilStatusController.BASE_PATH}/:id`, this.findByUnid);
        router.post(`${CivilStatusController.BASE_PATH}/search`, this.search);
        router.post(`${CivilStatusController.BASE_PATH}/integrity/:contractAddress`, this.checkIntegrity);
        router.post(CivilStatusController.BASE_PATH, this.register);
        router.put(CivilStatusController.BASE_PATH, this.update);
    }

    /**
     * Retrieve a civil state entry
     * 
     * @param request 
     * @param response 
     */
    private findByUnid(request: Request, response: Response): void {
        civilStatusService.findByUnid(request.params.id)
            .then((civilState) => {
                if(ObjectUtils.isNullOrUndefined(civilState)) {
                    response.sendStatus(404);
                } else {
                    response.send(civilState);
                }
            })
            .catch(() => response.sendStatus(401));
    }

    /**
     * Retrieve civil state entries by criterias
     * 
     * @param request 
     * @param response 
     */
    private search(request: Request, response: Response): void {
        civilStatusService.search(request.body)
            .then((civilStates) => {
                if(ArrayUtils.isEmpty(civilStates)) {
                    response.sendStatus(404);
                } else {
                    response.send(civilStates);
                }
            })
            .catch(() => response.sendStatus(401));
    }

    /**
     * Register person into civil state
     * 
     * @param request 
     * @param response 
     */
    private register(request: Request, response: Response): void {
        civilStatusService.add(request.body).then((civilStatus: CivilStatus) => {
            response.statusCode = 201;
            response.send(civilStatus.blockchainAddress);
        }).catch((reason) => {
            console.error(`Register civil status : ${reason}`);
            response.sendStatus(500);
        });
    }

    /**
     * Check civil state integrity with blockchain
     * 
     * @param request 
     * @param response 
     */
    private checkIntegrity(request: Request, response: Response): void {
        const person: Person = request.body;
        const contractAddress = request.params.contractAddress;

        civilStatusService.checkIntegrity(contractAddress, person).then((isValidIntegrity) => {
            response.statusCode = 200;
            response.send(isValidIntegrity);
        }).catch(() => response.send(500));
    }

    /**
     * Update person into civil state
     * 
     * @param request 
     * @param response 
     */
    private update(request: Request, response: Response): void {

    }
}