import { Request, Response, Router } from "express";
import { healthRecordBookletService } from "../../../core/services/impl/health-record-booklet.service";
import { AbstractController } from "../abstract.controller";

/**
 * The civil state api to add, update, delete entries into civil state
 */
export class HealthRecordBookletController extends AbstractController {
    private static readonly BASE_PATH: string = '/healthRecordBooklet';

    /**
     * Configure routing
     * 
     * @param router : the express router instance
     */
    configureRoutes(router: Router): void {
        router.post(HealthRecordBookletController.BASE_PATH, this.register);
    }

    /**
     * Register  new record into HealthRecordBooklet
     * 
     * @param request 
     * @param response 
     */
    private register(request: Request, response: Response): void {
        healthRecordBookletService.add(request.body).then(() => {
            response.sendStatus(201);
        }).catch((reason) => {
            console.error(`Register health record booklet : ${reason}`);
            response.sendStatus(500);
        });
    }


}