import { Router } from "express";

/**
 * Abstract controller
 */
export abstract class AbstractController {
    private router: Router;

    public constructor() {
        this.router = Router();
        this.configureRoutes(this.router);
    }

    /**
     * Configure all controller routes
     */
    abstract configureRoutes(router: Router) : void;

    /**
     * Get the controller router instance
     */
    getRouter(): Router {
        return this.router;
    }
}