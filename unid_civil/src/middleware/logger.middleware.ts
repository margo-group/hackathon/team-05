import * as express from 'express';

/**
 * The application logger middleware
 */
export class LoggerMiddleware {
    /**
     * Log any routes calls
     * 
     * @param request 
     * @param response 
     * @param next 
     */
    public static log(request: express.Request, response: express.Response, next: express.NextFunction) {
        console.log(`${request.method} ${request.path}`);
        next();
    }
}