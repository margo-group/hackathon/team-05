import {Router, Application} from "express";
import { AbstractController } from "../api/controllers/abstract.controller";
import { CivilStatusController } from "../api/controllers/civilState/civil-status.controller";
import { HealthRecordBookletController } from "../api/controllers/healthRecordBooklet/health-record-booklet-controller";

export class RoutesConfigurer {
    private router: Router = Router();

    // register all controllers
    private controllers: Array<AbstractController> = [
        new CivilStatusController(),
        new HealthRecordBookletController()
    ];

    public constructor(app: Application) {
        this.configure(app)
    }

    /**
     * Get the global application router
     */
    public getRouter(): Router {
        return this.router;
    }

    private configure(app: Application): void {
        this.controllers.forEach((controller: AbstractController) => {
            this.router.use('/', controller.getRouter())
        })

        // define /api base url and associate any created routes
        app.use('/api', this.router);
    }
}