// We will be using Solidity version 0.5.11
pragma solidity 0.5.11;

/**
 * @title Unid_Civil
 * @author Thomas Foutrein
 * @notice This contract is used to create civil status of the child, declared by a declarant like a midwife or a mayor or ...
 */


contract unid_civil {
    
    string public unid;
    string public civilStatusHash;
    string public motherCivilStatusHash;
    string public fatherCivilStatusHash;
    string public declarantId;
    string public firstWitnessCivilStatusHash;
    string public secondWitnessCivilStatusHash;
    
     /**
     * Contract constructor
     *
     * @param _unid the united :nation identitfy, a unique id if of the child defined by the unid-back
     * @param _civilStatusHash : hash of the civil status of the child grouping (firstName, lastName, birthdate, birthplace)
     * @param _motherCivilStatusHash : hash of the civil status of the child's mother (optional)
     * @param _fatherCivilStatusHash : hash of the civil status of the child's father (optional)
     * @param _declarantId : unique id of the person who have declared the child ()
     * @param _firstWitnessCivilStatusHash : hash of the civil status of the first witness of the child birth (optional)
     * @param _secondWitnessCivilStatusHash : hash of the civil status of the first witness of the child birth (optional)
     */

    constructor(string memory _unid,
                string memory _civilStatusHash,
                string memory _motherCivilStatusHash,
                string memory _fatherCivilStatusHash,
                string memory _declarantId,
                string memory _firstWitnessCivilStatusHash,
                string memory _secondWitnessCivilStatusHash) public {
        unid = _unid;
        civilStatusHash = _civilStatusHash;
        motherCivilStatusHash = _motherCivilStatusHash;
        fatherCivilStatusHash = _fatherCivilStatusHash;
        declarantId = _declarantId;
        firstWitnessCivilStatusHash = _firstWitnessCivilStatusHash;
        secondWitnessCivilStatusHash = _secondWitnessCivilStatusHash;
    }

    function setCivilStatusHash(string memory _civilStatusHash) public returns (bool success) {
        civilStatusHash = _civilStatusHash;
        return true;
    }

    function setMotherCivilStatusHash(string memory _motherCivilStatusHash) public returns (bool success) {
        motherCivilStatusHash = _motherCivilStatusHash;
        return true;
    }

    function setFatherCivilStatusHash(string memory _fatherCivilStatusHash) public returns (bool success) {
        fatherCivilStatusHash = _fatherCivilStatusHash;
        return true;
    }

    function setDeclarantId(string memory _declarantId) public returns (bool success) {
        declarantId = _declarantId;
        return true;
    }

    function setFirstWitnessCivilStatusHash(string memory _firstWitnessCivilStatusHash) public returns (bool success) {
        firstWitnessCivilStatusHash = _firstWitnessCivilStatusHash;
        return true;
    }

    function setSecondWitnessCivilStatusHash(string memory _secondWitnessCivilStatusHash) public returns (bool success) {
        secondWitnessCivilStatusHash = _secondWitnessCivilStatusHash;
        return true;
    }
}


