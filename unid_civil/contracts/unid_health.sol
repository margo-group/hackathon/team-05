// We will be using Solidity version 0.5.11
pragma solidity 0.5.11;

/**
 * @title Unid_Health
 * @author Thomas Foutrein
 * @notice This contract is used to create health record booklet for the child, registered by dispensary
 */


contract unid_health {
    
    string public unid;
    string public healthRecordBookletHash;

    
     /**
     * Contract constructor
     *
     * @param _unid the united :nation identitfy, a unique id if of the child defined by the unid-back
     * @param _healthRecordBookletHash : hash IPNS of the heath record booklet created on IPFS for the child
     */

    constructor(string memory _unid,
                string memory _healthRecordBookletHash) public {
        unid = _unid;
        healthRecordBookletHash = _healthRecordBookletHash;
    }
}