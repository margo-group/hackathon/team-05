// We will be using Solidity version 0.5.11
pragma solidity 0.5.11;

/**
 * @title Unid_Education
 * @author Thomas Foutrein
 * @notice This contract is used to create education record booklet for the child, registered by schools
 */


contract unid_education {
    
    string public unid;
    string public educationRecordBookletHash;

     /**
     * Contract constructor 
     *
     * @param _unid the united :nation identitfy, a unique id if of the child defined by the unid-back
     * @param _educationRecordBookletHash : hash IPNS (PeerId) of the education record booklet created on IPFS for the child
     */

    constructor(string memory _unid,
                string memory _educationRecordBookletHash) public {
        unid = _unid;
        educationRecordBookletHash = _educationRecordBookletHash;
    }
}