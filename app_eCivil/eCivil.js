const ISSUER_ID = "midwife-24647"

function getFormInfo() {
    
    let child = {}
    child.gender = document.getElementById('child_gender').value
    child.firstname = document.getElementById('child_firstname').value
    child.lastname = document.getElementById('child_lastname').value
    child.birthdate = document.getElementById('child_birthdate').value
    child.birthplace = document.getElementById('child_birthplace').value
    
    let mother = {}
    mother.gender = 'Female'
    mother.firstname = document.getElementById('mother_firstname').value
    mother.lastname = document.getElementById('mother_lastname').value
    mother.birthdate = document.getElementById('mother_birthdate').value
    mother.birthplace = document.getElementById('mother_birthplace').value

    let father = {}
    father.gender = 'Male'
    father.firstname = document.getElementById('father_firstname').value
    father.lastname = document.getElementById('father_lastname').value
    father.birthdate = document.getElementById('father_birthdate').value
    father.birthplace = document.getElementById('father_birthplace').value

    let witnesses = {}
    witnesses.first = document.getElementById('witnesses_first').value
    witnesses.second = document.getElementById('witnesses_second').value
    
    let record = {}
    record.child = child
    record.mother = mother
    record.father = father
    record.witnesses = witnesses
    record.issuerId = ISSUER_ID

    return record
}

function fillForm() {
    
    // child info
    document.getElementById('child_gender').value = 'Female'
    document.getElementById('child_firstname').value = 'Kira'
    document.getElementById('child_lastname').value = "MALINGO"
    document.getElementById('child_birthdate').value = '2019-09-28'
    document.getElementById('child_birthplace').value = 'Dakar'

    // mother
    document.getElementById('mother_firstname').value = 'Shiva'
    document.getElementById('mother_lastname').value = 'SONKO'
    document.getElementById('mother_birthdate').value = '1995-12-10'
    document.getElementById('mother_birthplace').value = 'Dakar'

    // father
    document.getElementById('father_firstname').value = "Maffal"
    document.getElementById('father_lastname').value = "MALINGO"
    document.getElementById('father_birthdate').value = '1982-09-05'
    document.getElementById('father_birthplace').value = 'Touba'

    // witnesses
    document.getElementById('witnesses_first').value = "Village Chief"
    document.getElementById('witnesses_second').value = "Abdou Ndiaye"
}

function submit() {
 let record = getFormInfo()

 $.ajax({
    
       url : 'http://192.168.2.151:3000/api/civilStatus',
       type : 'POST', 
       data : JSON.stringify(record),
       dataType : 'text',
       contentType: "application/json; charset=utf-8",
       success : function(code_html, statut){ // success est toujours en place, bien sûr !
	
	var qrcode = new QRCode("qrcode")
	qrcode.makeCode(code_html)}
                  ,


       error : function(resultat, statut, erreur){
	alert(erreur)

       }
    })

}
